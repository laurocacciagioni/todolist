var count = 0;
var Vista = Backbone.View.extend({
    el: '.vista',
    events: {
        'click .boton': 'agregar',
        'click .Eliminar': 'eliminar',
        'click #clear': 'agregar',
    },

    initialize: function () {
        this.render();
        if (typeof (Storage) !== "undefined") 
        {     
            count = localStorage.getItem("count");
        }
    },
    render: function () {
        
      /*  for (var key in localStorage) {
            if (key != "length" && key != "getItem" && key != "setItem" && key != "removeItem" && key != "clear"&&key !="key")
                $("#lista").append('<li class="ListItem" id="' +  key + '">' + key + ' <button class="Eliminar btn btn-secondary">-</button>' + '</li>');
        }*/

    },
    agregar: function () {
        if( $("#item").val()=="")return;
        if(isEmpty($("#item").val())){
            $("#item").val("");
            return;
        };
        count++;
        $("#lista").append('<li class="ListItem" id="' + count + '">' + $("#item").val() + ' <button class="Eliminar btn btn-secondary">-</button>' + '</li>');

       
        var objDiv = $("#"+count);
        if (objDiv[0] != undefined)
        $(objDiv)[0].scrollIntoView("smooth"); // [0] dom element of a jquery object
        var item = new Tarea({ name: $("#item").val()});
        Listado.add(item);
        


        if (typeof (Storage) !== "undefined") {
            localStorage.setItem("ToDoList",JSON.stringify(Listado.toJSON()));
            localStorage.setItem("count", count);
        }

        $("#item").val("");

    },
    eliminar: function (ev) {
        $(ev.target).closest('li').remove();        
        var nombre =$(ev.target).closest('li').attr("id");
        Listado.eliminarItem(nombre);
        Listado.remove(Listado.find(function(model) { return model.get('name') === nombre; }));

        if (typeof (Storage) !== "undefined") {
            localStorage.setItem("ToDoList",JSON.stringify(Listado.toJSON()));
        }
        //localStorage.removeItem($(ev.target).closest('li').attr("id"));
    },
    
    

});
var view = new Vista();



function limpiar()
{
    localStorage.clear();
    $(".ListItem").remove();
    $('#confirmModal').modal('hide');
}
function isEmpty(str){
    return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
}
